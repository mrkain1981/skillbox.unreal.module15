﻿// HomeTask15.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

/*void printEvenOrOdd(bool bEven, int n)
{
    // bEven: если = true, то выводит чётные, иначе - нечётные. Ноль считается чётным числом

    for (int i = 0; i <= n; i++)
    {
        if (bEven && (i % 2 == 0))
        {
            std::cout << i << "\n";
        } 
        else if (!bEven && (i % 2 != 0))
        {
            std::cout << i << "\n";
        }
    }
}*/

void printEvenOrOdd(bool bEven, int n)
{
    // bEven: если = true, то выводит чётные, иначе - нечётные. Ноль считается чётным числом

    for (int i = !bEven; i <= n; i += 2)
    {
            std::cout << i << "\n";
    }
}

int main()
{
    const int N = 15;

    for (int i = 0; i <= N; i++)
    {
        // Обычно считается, что ноль является чётным числом, поэтому условие будет включать и его
        if (i % 2 == 0) 
        {
            std::cout << i << "\n";
        }
    }

    std::cout << "Print Evens:\n";
    printEvenOrOdd(true, N);

    std::cout << "Print Odds:\n";
    printEvenOrOdd(false, N);
}



// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
